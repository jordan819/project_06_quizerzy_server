package pl.edu.pwsztar.quiz.domain.dto;


import com.opencsv.bean.CsvBindByPosition;



public class QuestionDto {

    @CsvBindByPosition(position = 0)
    private String id;

    @CsvBindByPosition(position = 1)
    private String question;

    @CsvBindByPosition(position = 2)
    private String answer1;

    @CsvBindByPosition(position = 3)
    private String answer2;

    @CsvBindByPosition(position = 4)
    private String answer3;

    @CsvBindByPosition(position = 5)
    private String answer4;

    @CsvBindByPosition(position = 6)
    private String correctAnswers;

    @CsvBindByPosition(position = 7)
    private String points;


    public String getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer1() {
        return answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public int[] getCorrectAnswers() {
        String[] answers = correctAnswers.split(",");

        int[] answersAsIntegers = new int[answers.length];

        for (int i=0; i<answers.length; i++) {
            answersAsIntegers[i] = Integer.parseInt(answers[i]);
        }

        return answersAsIntegers;
    }

    public String getPoints() {
        return points;
    }
}
