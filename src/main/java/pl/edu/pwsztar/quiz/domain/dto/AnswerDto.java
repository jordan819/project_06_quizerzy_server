package pl.edu.pwsztar.quiz.domain.dto;

public class AnswerDto {

    private int questionId;
    private int[] selectedAnswers;

    public AnswerDto() {
    }

    public int getQuestionId() {
        return questionId;
    }

    public int[] getSelectedAnswers() {
        return selectedAnswers;
    }

}
