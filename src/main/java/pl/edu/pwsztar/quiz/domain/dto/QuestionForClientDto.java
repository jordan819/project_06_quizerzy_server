package pl.edu.pwsztar.quiz.domain.dto;

public class QuestionForClientDto {

    private final String question;
    private final String[] answers;
    private final int points;
    private final boolean lastQuestion;

    public QuestionForClientDto(String question, String[] answers, int points, boolean lastQuestion) {
        this.question = question;
        this.answers = answers;
        this.points = points;
        this.lastQuestion = lastQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public String[] getAnswers() {
        return answers;
    }

    public int getPoints() {
        return points;
    }

    public boolean isLastQuestion() {
        return lastQuestion;
    }
}
