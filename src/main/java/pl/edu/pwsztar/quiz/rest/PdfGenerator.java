package pl.edu.pwsztar.quiz.rest;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.ZonedDateTime;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.pwsztar.quiz.domain.dto.QuestionDto;


public class PdfGenerator {

    private static final String PATH = "C:\\temp\\";

    private static final String ARIAL = "src\\main\\resources\\fonts\\arial.ttf";

    private static java.util.List<QuestionDto> questions;
    private static java.util.List<int[]> usersAnswers;

    private static int score;
    private static int maxScore;
    private static int requiredScore;

    private static final Logger LOGGER = LoggerFactory.getLogger(PdfGenerator.class);

    public boolean createReport(java.util.List<QuestionDto> questions, java.util.List<int[]> usersAnswers,
                             int score, int maxScore, int requiredScore) throws IOException {

        LOGGER.info("Tworzę raport...");

        Files.createDirectories(Paths.get(PATH));

        PdfGenerator.score = score;
        PdfGenerator.maxScore = maxScore;
        PdfGenerator.requiredScore = requiredScore;

        try {
            PdfGenerator.questions = questions;
            PdfGenerator.usersAnswers = usersAnswers;

            Document document = new Document();
            String fileName = "raport" + ZonedDateTime.now().toEpochSecond() + ".pdf";
            PdfWriter.getInstance(document, new FileOutputStream(PATH + fileName));
            document.open();
            addContent(document);
            document.close();
            LOGGER.info("Plik {} został zapisany w lokalizacji {}", fileName, PATH);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isPassed();
    }

    private static void addContent(Document document) throws DocumentException, IOException {

        addEmptyLine(document, 4);
        setBackground(document);
        createHeader(document);
        addEmptyLine(document, 4);
        createTable(document);

    }

    private static void createTable(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(3);

        PdfPCell c1 = new PdfPCell(new Phrase("Pytanie"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Twoje odpowiedzi"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Poprawne odpowiedzi"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.setHeaderRows(1);


        Font font = FontFactory.getFont(ARIAL, BaseFont.IDENTITY_H, true);
        for (int i=0; i<questions.size(); i++) {
            PdfPCell cell = new PdfPCell(new Phrase(questions.get(i).getQuestion(), font));

            table.addCell(cell);
            cell = new PdfPCell();

            String[] answers = new String[4];
            answers[0] = questions.get(i).getAnswer1();
            answers[1] = questions.get(i).getAnswer2();
            answers[2] = questions.get(i).getAnswer3();
            answers[3] = questions.get(i).getAnswer4();
            int[] correctAnswers = questions.get(i).getCorrectAnswers();
            for (int j=0; j<usersAnswers.get(i).length; j++) {
                int index = usersAnswers.get(i)[j] - 1;

                for (int correctAnswer : correctAnswers) {
                    int index2 = correctAnswer - 1;
                    if (index == index2) {
                        font.setColor(BaseColor.GREEN);
                        break;
                    } else {
                        font.setColor(BaseColor.RED);
                    }
                }
                cell.addElement(new Phrase(answers[index], font));

            }
            table.addCell(cell);
            font.setColor(BaseColor.BLACK);

            StringBuilder correctAnswersAsText = new StringBuilder();
            for (int correctAnswer : correctAnswers) {
                int index = correctAnswer - 1;
                correctAnswersAsText.append(answers[index]).append("\n");
            }
            table.addCell(correctAnswersAsText.toString());

        }

        document.add(table);

    }

    private static void createHeader(Document document) throws DocumentException {
        Font font = FontFactory.getFont(ARIAL, BaseFont.IDENTITY_H, true);
        Paragraph paragraph;

        font.setSize(32);
        if (isPassed()) {
            font.setColor(BaseColor.GREEN);
            paragraph = new Paragraph("QUIZ ZALICZONY", font);
        } else {
            font.setColor(BaseColor.RED);
            paragraph = new Paragraph("QUIZ NIEZALICZONY", font);
        }
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);

        addEmptyLine(document, 1);

        font.setSize(-1);
        font.setColor(BaseColor.BLACK);
        paragraph = new Paragraph("Wynik: " + score + "/" + maxScore, font);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);
    }

    private static boolean isPassed() {
        return score > requiredScore;
    }

    private static void addEmptyLine(Document document, int number) throws DocumentException {
        for (int i = 0; i < number; i++) {
            document.add(new Paragraph(" "));
        }
    }

    private static void setBackground(Document document) throws DocumentException, IOException {
        Image jpeg = Image.getInstance("src\\main\\resources\\images\\background.jpg");
        jpeg.setAlignment(Image.UNDERLYING);
        jpeg.setAbsolutePosition(0, 0);
        jpeg.scaleAbsolute(595, 842);
        document.add(jpeg);
    }
}