package pl.edu.pwsztar.quiz.rest;

import com.opencsv.bean.CsvToBeanBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwsztar.quiz.domain.dto.AnswerDto;
import pl.edu.pwsztar.quiz.domain.dto.QuestionDto;
import pl.edu.pwsztar.quiz.domain.dto.QuestionForClientDto;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/quiz")
public class QuizApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuizApiController.class);

    private final List<QuestionDto> questions = readQuestions();
    private List<int[]> usersAnswers;
    private int score = 0;

    private final int MAX_SCORE = 6;
    private final int REQUIRED_SCORE = (int) Math.floor(MAX_SCORE * 0.5);

    private List<QuestionDto> readQuestions() {

        LOGGER.info("Działa metoda 'readQuestions'");

        final String fileName = "src\\main\\resources\\questions.csv";

        List<QuestionDto> questions = null;

        try {
            questions = new CsvToBeanBuilder(new FileReader(fileName))
                    .withType(QuestionDto.class)
                    .withSeparator(';')
                    .withSkipLines(1)
                    .build().parse();

            for (QuestionDto question: questions) {
                LOGGER.info("Wczytano linijkę: {} {} {} {} {} {} {}",
                        question.getQuestion(),
                        question.getAnswer1(),
                        question.getAnswer2(),
                        question.getAnswer3(),
                        question.getAnswer4(),
                        question.getCorrectAnswers(),
                        question.getPoints());
            }
            
        } catch (FileNotFoundException e) {
            LOGGER.info("Plik z pytaniami {} nie został znaleziony!", fileName);
        }
        
        return questions;
        }

    @GetMapping(value = "test")
    public ResponseEntity<Void> testServer() {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "question/{id}", produces={"application/json; charset=UTF-8"})
    public ResponseEntity<QuestionForClientDto> getQuestion(@PathVariable("id") String id) {
        LOGGER.info("Działa metoda getQuestion z parameterem id: {}", id);

        QuestionDto questionDto;
        QuestionForClientDto data;

        try {
            questionDto = questions.get(Integer.parseInt(id)-1);

            String question = questionDto.getQuestion();

            String[] answers = new String[4];
            answers[0] = questionDto.getAnswer1();
            answers[1] = questionDto.getAnswer2();
            answers[2] = questionDto.getAnswer3();
            answers[3] = questionDto.getAnswer4();

            int points = Integer.parseInt(questionDto.getPoints());

            boolean lastQuestion = Integer.parseInt(id) == questions.size();


            data = new QuestionForClientDto(question, answers, points, lastQuestion);

        } catch (IndexOutOfBoundsException e) {
            LOGGER.warn("Pytanie z id: {} nie istnieje", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @PutMapping(value = "calculate")
    public ResponseEntity<Void> calculate(@RequestBody AnswerDto answerDto) {

        int id = answerDto.getQuestionId()-1;
        int[] selectedAnswers = answerDto.getSelectedAnswers();

        int[] correctAnswers = questions.get(id).getCorrectAnswers();

        if (id == 0) {

            usersAnswers = new ArrayList<int[]>();
            score = 0;
        }

        usersAnswers.add(selectedAnswers);

        LOGGER.info("Działa metoda calculate z danymi: id: {}, selectedAnswers: {}",
                id, selectedAnswers);

        //sprawdzanie poprawności odpowiedzi
        if (Arrays.equals(selectedAnswers, correctAnswers)) {
            int points = Integer.parseInt(questions.get(id).getPoints());
            score += points;
            LOGGER.info("Przyznano punktów: {} za odpowiedź, suma punktów: {}", points, score);
        } else {
            LOGGER.info("Odpowiedź niepoprawna, suma punktów: {}", score);
        }


        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "report")
    public ResponseEntity<Void> createReport() throws IOException {

        LOGGER.info("Działa metoda createReport");

        boolean isPassed = new PdfGenerator().createReport(questions, usersAnswers, score, MAX_SCORE, REQUIRED_SCORE);

        if (isPassed) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

}
